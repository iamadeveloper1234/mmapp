const accountSid = 'AC40249e30d97a9db562e3bac575491c06';
const authToken = '2765b3de48729e1ddbfd5e473550be47';
const client = require('twilio')(accountSid, authToken);

client.messages
  .create({
     body: 'This is the ship that made the Kessel Run in fourteen parsecs?',
     from: '+15122336125',
     to: '+12148376376'
   })
  .then(message => console.log(message.sid))
  .done();
